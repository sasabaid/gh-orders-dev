import React, { useEffect, useState } from 'react';
import { makeStyles } from '@mui/styles';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    width: '100%',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '100%',
  },
  textArea: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '100%',
  },
}));

const CompanyCreate = ({
  open,
  companyData,
  handleClose,
  handleCompanySave,
}) => {
  const [name, setName] = useState('');
  const [gst, setGst] = useState('');
  const [address, setAddress] = useState('');

  useEffect(() => {
    if (companyData) {
      setName(companyData.name);
      setGst(companyData.gst);
      setAddress(companyData.address);
    }
  }, [companyData]);

  const classes = useStyles();

  const handleReset = () => {
    setName('');
    setGst('');
    setAddress('');
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      fullWidth={true}
      maxWidth={'sm'}
    >
      <DialogTitle id="alert-dialog-title">{'COMPANY DETAILS'}</DialogTitle>
      <DialogContent>
        <form className={classes.container} autoComplete="off">
          <div>
            <TextField
              id="name"
              className={classes.textField}
              label="Name"
              margin="normal"
              value={name}
              onChange={(event) => setName(event.target.value)}
              autoFocus
            />
          </div>
          <div>
            <TextField
              id="gst"
              label="GST"
              value={gst}
              className={classes.textField}
              margin="normal"
              onChange={(event) => setGst(event.target.value)}
              autoComplete="off"
            />
          </div>
          <div>
            <TextField
              id="address"
              label="Address"
              multiline
              rows="4"
              value={address}
              className={classes.textArea}
              margin="normal"
              onChange={(event) => setAddress(event.target.value)}
              autoComplete="off"
            />
          </div>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleReset} color="primary">
          RESET
        </Button>
        <Button
          onClick={() =>
            handleCompanySave({
              name,
              gst,
              address,
            })
          }
          color="primary"
          autoFocus
          disabled={name === '' || address === '' || gst == ''}
        >
          SAVE
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CompanyCreate;
