import React from 'react';
import Login from './Login';
import OrdersList from './OrdersList';
import './App.css';
import ForgotPassword from './ForgotPassword';

const App = () => {
  const [showOrders, setShowOrders] = React.useState(false);
  const [showForgotPassword, setShowForgotPassword] = React.useState(false);

  const handlePasswordCheck = (result) => {
    setShowOrders(result);
  };

  const handleForgotPassword = result => {
    setShowForgotPassword(result);
  };

  return (
    <div className="App">
      {showOrders && <OrdersList />}
      {!showOrders && !showForgotPassword && (
        <Login
          handlePasswordCheck={handlePasswordCheck}
          handleForgotPassword={handleForgotPassword}
        />
      )}
      {showForgotPassword && (
        <ForgotPassword handleForgotPassword={handleForgotPassword} />
      )}
    </div>
  );
};

export default App;
