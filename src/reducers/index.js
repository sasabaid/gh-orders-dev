import { combineReducers } from 'redux';
import ordersReducer from './ordersReducer';
import billReducer from './billReducer';
import comapnyReducer from './companyReducer';
import productsReducer from './productsReducer';

const reducers = combineReducers({
  ordersReducer,
  billsInfo: billReducer,
  companyInfo: comapnyReducer,
  productsInfo: productsReducer,
});

export default reducers;
