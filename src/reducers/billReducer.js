import actions from '../actionTypes';

const defaultState = {
  bills: [],
  updatedBill: {},
  deletedBill: {},
  allBillsInProgress: false,
  saveBillInProgress: false,
  updateBillInProgress: false,
  deleteBillInProgress: false,
};

const billReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.ALL_BILLS:
      return Object.assign({}, state, {
        allBillsInProgress: true,
      });
    case actions.ALL_BILLS_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          bills: action.response,
        },
        {
          allBillsInProgress: false,
        },
      );
    case actions.ALL_BILLS_FAILURE:
      return Object.assign(
        {},
        state,
        {
          allBillsFailure: action.error,
        },
        {
          allBillsInProgress: false,
        },
      );
    case actions.SAVE_BILL:
      return Object.assign({}, state, {
        saveBillInProgress: true,
      });
    case actions.SAVE_BILL_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          bills: [...state.bills, action.response],
        },
        {
          saveBillInProgress: false,
        },
      );
    case actions.SAVE_BILL_FAILURE:
      return Object.assign(
        {},
        state,
        {
          saveBillFailure: action.error,
        },
        {
          saveBillInProgress: false,
        },
      );
    case actions.UPDATE_BILL:
      return Object.assign({}, state, {
        updateBillInProgress: true,
      });
    case actions.UPDATE_BILL_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          updatedBill: action.response,
        },
        {
          updateBillInProgress: false,
        },
      );
    case actions.UPDATE_BILL_FAILURE:
      return Object.assign(
        {},
        state,
        {
          updatedBillFailure: action.error,
        },
        {
          updateBillInProgress: false,
        },
      );
    case actions.DELETE_BILL:
      return Object.assign({}, state, {
        deleteBillInProgress: true,
      });
    case actions.DELETE_BILL_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          deletedBill: action.response,
        },
        {
          deleteBillInProgress: false,
        },
      );
    case actions.DELETE_BILL_FAILURE:
      return Object.assign(
        {},
        state,
        {
          deletedBillFailure: action.error,
        },
        {
          deleteBillInProgress: false,
        },
      );
    default:
      return state;
  }
};

export default billReducer;
