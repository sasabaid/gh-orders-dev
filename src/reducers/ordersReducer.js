import actions from '../actionTypes';

const defaultState = {
  orders: [],
  updatedOrder: {},
  deletedOrder: {},
  allOdersInProgress: false,
  saveOrderInProgress: false,
  updateOrderInProgress: false,
  deleteOrderInProgress: false,
  searchedOrder: [],
};

const ordersReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.ALL_OPEN_ORDERS:
      return Object.assign({}, state, {
        allOdersInProgress: true,
      });
    case actions.ALL_OPEN_ORDERS_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          orders: action.response,
        },
        {
          allOdersInProgress: false,
        },
      );
    case actions.ALL_OPEN_ORDERS_FAILURE:
      return Object.assign(
        {},
        state,
        {
          allOrdersFailure: action.error,
        },
        {
          allOdersInProgress: false,
        },
      );
    case actions.SAVE_ORDER:
      return Object.assign({}, state, {
        saveOrderInProgress: true,
      });
    case actions.SAVE_ORDER_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          orders: [...state.orders, action.response],
        },
        {
          saveOrderInProgress: false,
        },
      );
    case actions.SAVE_ORDER_FAILURE:
      return Object.assign(
        {},
        state,
        {
          saveOrderFailure: action.error,
        },
        {
          saveOrderInProgress: false,
        },
      );
    case actions.UPDATE_ORDER:
      return Object.assign({}, state, {
        updateOrderInProgress: true,
      });
    case actions.UPDATE_ORDER_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          updatedOrder: action.response,
        },
        {
          updateOrderInProgress: false,
        },
      );
    case actions.UPDATE_ORDER_FAILURE:
      return Object.assign(
        {},
        state,
        {
          updatedOrderFailure: action.error,
        },
        {
          updateOrderInProgress: false,
        },
      );
    case actions.DELETE_ORDER:
      return Object.assign({}, state, {
        deleteOrderInProgress: true,
      });
    case actions.DELETE_ORDER_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          deletedOrder: action.response,
        },
        {
          deleteOrderInProgress: false,
        },
      );
    case actions.DELETE_ORDER_FAILURE:
      return Object.assign(
        {},
        state,
        {
          deletedOrderFailure: action.error,
        },
        {
          deleteOrderInProgress: false,
        },
      );
    case actions.SEARCH_ORDER_SUCCESS:
      return Object.assign({}, state, {
        searchedOrder: action.response,
      });
    case actions.SEARCH_ORDER_FAILURE:
      return Object.assign({}, state, {
        searchedOrder: [],
      });
    case actions.SEARCH_ORDER_RESET:
      return Object.assign({}, state, {
        searchedOrder: [],
      });
    default:
      return state;
  }
};

export default ordersReducer;
