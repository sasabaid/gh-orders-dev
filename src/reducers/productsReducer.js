import actions from '../actionTypes';

const defaultState = {
  productsList: [],
  updatedProduct: {},
  deletedProduct: {},
  allProductsInProgress: false,
  saveProductInProgress: false,
  updateProductInProgress: false,
  deleteProductInProgress: false,
};

const productsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.ALL_PRODUCTS:
      return Object.assign({}, state, {
        allProductsInProgress: true,
      });
    case actions.ALL_PRODUCTS_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          productsList: action.response,
        },
        {
          allProductsInProgress: false,
        },
      );
    case actions.ALL_PRODUCTS_FAILURE:
      return Object.assign(
        {},
        state,
        {
          allProductsFailure: action.error,
        },
        {
          allProductsInProgress: false,
        },
      );
    case actions.SAVE_PRODUCT:
      return Object.assign({}, state, {
        saveProductInProgress: true,
      });
    case actions.SAVE_PRODUCT_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          productsList: [...state.productsList, action.response],
        },
        {
          saveProductInProgress: false,
        },
      );
    case actions.SAVE_PRODUCT_FAILURE:
      return Object.assign(
        {},
        state,
        {
          saveProductFailure: action.error,
        },
        {
          saveProductInProgress: false,
        },
      );
    case actions.UPDATE_PRODUCT:
      return Object.assign({}, state, {
        updateProductInProgress: true,
      });
    case actions.UPDATE_PRODUCT_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          updatedProduct: action.response,
        },
        {
          updateProductInProgress: false,
        },
      );
    case actions.UPDATE_PRODUCT_FAILURE:
      return Object.assign(
        {},
        state,
        {
          updatedProductFailure: action.error,
        },
        {
          updateProductInProgress: false,
        },
      );
    case actions.DELETE_PRODUCT:
      return Object.assign({}, state, {
        deleteProductInProgress: true,
      });
    case actions.DELETE_PRODUCT_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          deletedProduct: action.response,
        },
        {
          deleteProductInProgress: false,
        },
      );
    case actions.DELETE_PRODUCT_FAILURE:
      return Object.assign(
        {},
        state,
        {
          deletedProductFailure: action.error,
        },
        {
          deleteProductInProgress: false,
        },
      );
    default:
      return state;
  }
};

export default productsReducer;
