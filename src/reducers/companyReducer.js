import actions from '../actionTypes';

const defaultState = {
  companies: [],
  updatedCompany: {},
  deletedCompany: {},
  allCompanysInProgress: false,
  saveCompanyInProgress: false,
  updateCompanyInProgress: false,
  deleteCompanyInProgress: false,
};

const companyReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.ALL_COMPANYS:
      return Object.assign({}, state, {
        allCompanysInProgress: true,
      });
    case actions.ALL_COMPANYS_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          companies: action.response,
        },
        {
          allCompanysInProgress: false,
        },
      );
    case actions.ALL_COMPANYS_FAILURE:
      return Object.assign(
        {},
        state,
        {
          allCompanysFailure: action.error,
        },
        {
          allCompanysInProgress: false,
        },
      );
    case actions.SAVE_COMPANY:
      return Object.assign({}, state, {
        saveCompanyInProgress: true,
      });
    case actions.SAVE_COMPANY_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          companies: [...state.companies, action.response],
        },
        {
          saveCompanyInProgress: false,
        },
      );
    case actions.SAVE_COMPANY_FAILURE:
      return Object.assign(
        {},
        state,
        {
          saveCompanyFailure: action.error,
        },
        {
          saveCompanyInProgress: false,
        },
      );
    case actions.UPDATE_COMPANY:
      return Object.assign({}, state, {
        updateCompanyInProgress: true,
      });
    case actions.UPDATE_COMPANY_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          updatedCompany: action.response,
        },
        {
          updateCompanyInProgress: false,
        },
      );
    case actions.UPDATE_COMPANY_FAILURE:
      return Object.assign(
        {},
        state,
        {
          updatedCompanyFailure: action.error,
        },
        {
          updateCompanyInProgress: false,
        },
      );
    case actions.DELETE_COMPANY:
      return Object.assign({}, state, {
        deleteCompanyInProgress: true,
      });
    case actions.DELETE_COMPANY_SUCCESS:
      return Object.assign(
        {},
        state,
        {
          deletedCompany: action.response,
        },
        {
          deleteCompanyInProgress: false,
        },
      );
    case actions.DELETE_COMPANY_FAILURE:
      return Object.assign(
        {},
        state,
        {
          deletedCompanyFailure: action.error,
        },
        {
          deleteCompanyInProgress: false,
        },
      );
    default:
      return state;
  }
};

export default companyReducer;
