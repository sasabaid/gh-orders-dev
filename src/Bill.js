import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import ItemsListing from './ItemsListing';
import TableCart from './TableCart';
import ProductCreate from './ProductCreate';
import { fuzzy_match } from './utils/GeneralUtils';
import { useDispatch, useSelector } from 'react-redux';
import actions from './actionTypes';

import './bill.css';

const PRODUCT_META_DATA = {
  name: '',
  hsn: '',
};

const Bill = ({ setDataAfterSubmit, billingItems }) => {
  const [selectedItem, setSelectedItem] = useState({});
  const [selectedIndex, setSelectedIndex] = useState(1);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [products, setProducts] = useState([]);
  const [productData, setProductData] = useState(null);
  const [openCreateProduct, setOpenCreateProduct] = useState(false);
  const [productAction, setProductAction] = useState('');

  const dispatch = useDispatch();

  const { productsList } = useSelector((state) => state.productsInfo);

  useEffect(() => {
    setProducts(productsList);
    setFilteredProducts(productsList);
  }, [productsList]);

  const handleListItemClick = (event, item) => {
    setSelectedItem(item);
  };

  const handleRowSelection = ({ target }, rowId) => {
    setSelectedIndex(rowId);
    setSelectedItem({});
    if (target.value) {
      searchAndSetFilteredProducts(target.value);
    } else {
      setFilteredProducts(products);
    }
  };

  const searchAndSetFilteredProducts = (searchTerm) => {
    let filteredResult = [];
    products.forEach((product) => {
      const result = fuzzy_match(product.name, searchTerm);
      if (result !== '') {
        filteredResult.push(product);
      }
    });
    setFilteredProducts(filteredResult);
  };

  const handleProductNameChange = ({ target }) => {
    searchAndSetFilteredProducts(target.value);
  };

  const handleProductModify = (action, data) => {
    setOpenCreateProduct(true);
    setProductAction(action);
    setProductData(data);
  };

  const handleCloseOfCreateProducts = () => {
    setOpenCreateProduct(false);
    setProductAction('');
    setProductData(null);
  };

  const handleCreateProductSave = (data) => {
    if (productAction === 'add') {
      dispatch({
        type: actions.SAVE_PRODUCT,
        payload: data,
      });
    } else {
      dispatch({
        type: actions.UPDATE_PRODUCT,
        payload: {
          id: productData._id,
          modifiedInfo: data,
        },
      });
    }
    handleCloseOfCreateProducts();
  };

  return (
    <>
      <div className={'bill-container'}>
        <div className={'item-listing-container'}>
          <ItemsListing
            title={'Products'}
            items={filteredProducts}
            selectedItem={selectedItem}
            handleListItemClick={handleListItemClick}
            handleEditItem={handleProductModify}
          />
          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={() => handleProductModify('add', PRODUCT_META_DATA)}
            >
              Add Product
            </Button>
          </div>
        </div>

        <TableCart
          selectedIndex={selectedIndex}
          selectedItem={selectedItem}
          handleProductNameChange={handleProductNameChange}
          handleRowSelection={handleRowSelection}
          setDataAfterSubmit={setDataAfterSubmit}
          billItems={billingItems}
        />
      </div>
      {openCreateProduct && (
        <ProductCreate
          handleProductSave={handleCreateProductSave}
          open={openCreateProduct}
          handleClose={handleCloseOfCreateProducts}
          productData={productData}
        />
      )}
    </>
  );
};

export default Bill;
