import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux';
import store, { sagaMiddleware } from './store';
import saga from './sagas/sagas';
import {
  createTheme,
  ThemeProvider as MuiThemeProvider,
} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import * as serviceWorker from './serviceWorker';
import Routes from './routes';

// function getQueryVariable(variable) {
//   if (window.location.search && window.location.search.length > 1) {
//     const query = window.location.search.substring(1);
//     const vars = query.split('&');
//     for (let i = 0; i < vars.length; i += 1) {
//       const pair = vars[i].split('=');
//       if (decodeURIComponent(pair[0]) === variable) {
//         return decodeURIComponent(pair[1]);
//       }
//     }
//   }
//   return '';
// }

const theme = createTheme({
  typography: {
    useNextVariants: true,
    fontFamily: ['Lato'].join(','),
    font: {
      color: '#333940',
    },
  },
  root: {
    color: '#333940',
  },
  palette: {
    tertiary: { main: '#004261' },
    primary: { main: '#1B6C92' },
    secondary: { main: '#00B4D2' },
    background: { default: '#fcfcfc' },
    error: { main: '#E10C32' },
    success: { main: '#00AB84' },
  },
  overrides: {
    MuiPaper: {
      root: {
        backgroundColor: '#ffffff',
        color: '#333940',
      },
    },
    App: {
      content: {
        padding: '0px',
      },
    },
    MuiFormLabel: {
      root: {
        width: 'max-content',
      },
      asterisk: {
        color: '#db3131',
        '&$error': {
          color: '#db3131',
        },
      },
    },
  },
});

sagaMiddleware.run(saga);

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <Routes />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root'));

serviceWorker.unregister();
