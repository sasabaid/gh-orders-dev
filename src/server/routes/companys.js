const express = require('express');
const router = express.Router();
const Companys = require('../models/companys');
const R = require('ramda');
const moment = require('moment');

router.get('/', (req, res) => {
  Companys.find()
    .then((allCompanys) => res.json(allCompanys))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.post('/', async (req, res) => {
  const companys = new Companys(req.body);
  try {
    const savedCompanys = await companys.save();
    res.json(savedCompanys);
  } catch (err) {
    res.json({
      message: err,
    });
  }
});

router.delete('/:companyId', (req, res) => {
  Companys.deleteOne({
    _id: req.params.companyId,
  })
    .then((data) => res.json(data))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.put('/:companyId', (req, res) => {
  Companys.updateOne(
    {
      _id: req.params.companyId,
    },
    {
      $set: req.body,
    },
  )
    .then((data) => res.json(data))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

module.exports = router;
