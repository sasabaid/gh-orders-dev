const express = require('express');
const router = express.Router();
const Products = require('../models/products');
const R = require('ramda');
const moment = require('moment');

router.get('/', (req, res) => {
  Products.find()
    .then((allProducts) => res.json(allProducts))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.post('/', async (req, res) => {
  const products = new Products(req.body);
  try {
    const savedProducts = await products.save();
    res.json(savedProducts);
  } catch (err) {
    res.json({
      message: err,
    });
  }
});

router.delete('/:productId', (req, res) => {
  Products.deleteOne({
    _id: req.params.productId,
  })
    .then((data) => res.json(data))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.put('/:productId', (req, res) => {
  Products.updateOne(
    {
      _id: req.params.productId,
    },
    {
      $set: req.body,
    },
  )
    .then((data) => res.json(data))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

module.exports = router;
