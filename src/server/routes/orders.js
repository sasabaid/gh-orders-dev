const express = require('express');
const router = express.Router();
const Orders = require('../models/orders');
const R = require('ramda');
const moment = require('moment');

router.get('/', (req, res) => {
  Orders.find()
    .then((allOrders) => res.json(allOrders))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.get('/open', (req, res) => {
  Orders.find({ status: 0 })
    .sort({ createdAt: 'desc' })
    .then((allOpenOrders) => res.json(allOpenOrders))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.get('/closed', (req, res) => {
  Orders.find({
    status: 1,
    updatedAt: {
      $gte: new Date(moment().format('YYYY-MM-DD')),
      $lt: new Date(moment().add(1, 'days').format('YYYY-MM-DD')),
    },
  })
    .then((allClosedOrders) => res.json(R.map(R.prop('name'), allClosedOrders)))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.get('/from_open', (req, res) => {
  Orders.find({ status: 0, type: 'from' })
    .then((allOpenFromOrders) => {
      let modifiedResponse = [];
      allOpenFromOrders.forEach((order) => {
        let daysPassed = moment(new Date()).diff(
          moment(new Date(order.createdAt)),
          'days',
        );
        modifiedResponse.push({
          name: order.name,
          details: order.details,
          daysPassed,
        });
      });
      res.json(modifiedResponse);
    })
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.get('/to_open', (req, res) => {
  Orders.find({ status: 0, type: 'to' })
    .then((allOpenToOrders) => {
      let modifiedResponse = [];
      allOpenToOrders.forEach((order) => {
        let daysPassed = moment(new Date()).diff(
          moment(new Date(order.createdAt)),
          'days',
        );
        modifiedResponse.push({
          name: order.name,
          details: order.details,
          daysPassed,
        });
      });
      res.json(modifiedResponse);
    })
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.post('/', async (req, res) => {
  const orders = new Orders(req.body);
  try {
    const savedOrders = await orders.save();
    res.json(savedOrders);
  } catch (err) {
    res.json({
      message: err,
    });
  }
});

router.get('/:orderId', (req, res) => {
  if (req.query.searchData !== undefined) {
    let searchString = req.query.searchData;
    Orders.find({
      $text: {
        $search: searchString,
      },
    })
      .then((foundOrders) => res.json(foundOrders))
      .catch((err) =>
        res.json({
          message: err,
        }),
      );
  } else {
    Orders.findOne({
      _id: req.params.orderId,
    })
      .then((foundOrder) => res.json(foundOrder))
      .catch((err) =>
        res.json({
          message: err,
        }),
      );
  }
});

router.delete('/:orderId', (req, res) => {
  Orders.deleteOne({
    _id: req.params.orderId,
  })
    .then((data) => res.json(data))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.put('/:orderId', (req, res) => {
  Orders.updateOne(
    {
      _id: req.params.orderId,
    },
    {
      $set: req.body,
    },
  )
    .then((data) => res.json(data))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

module.exports = router;
