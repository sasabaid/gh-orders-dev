const express = require('express');
const router = express.Router();
const Bills = require('../models/bills');
const R = require('ramda');
const moment = require('moment');

router.get('/', (req, res) => {
  Bills.find()
    .then((allBills) => res.json(allBills))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.post('/', async (req, res) => {
  const bills = new Bills(req.body);
  try {
    const savedBills = await bills.save();
    res.json(savedBills);
  } catch (err) {
    res.json({
      message: err,
    });
  }
});

router.delete('/:billId', (req, res) => {
  Bills.deleteOne({
    _id: req.params.billId,
  })
    .then((data) => res.json(data))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

router.put('/:billId', (req, res) => {
  Bills.updateOne(
    {
      _id: req.params.billId,
    },
    {
      $set: req.body,
    },
  )
    .then((data) => res.json(data))
    .catch((err) =>
      res.json({
        message: err,
      }),
    );
});

module.exports = router;
