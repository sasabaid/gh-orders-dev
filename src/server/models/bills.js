const mongoose = require('mongoose');

const BillsSchema = mongoose.Schema({
  billingItems: {
    type: Object,
    required: true,
  },
  companyInfo: {
    type: Object,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('Bills', BillsSchema);
