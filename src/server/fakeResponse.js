const states = [
  {
    state: 'Alaska',
    code: 'AK',
  },
];

exports.data = {
  states,
};
