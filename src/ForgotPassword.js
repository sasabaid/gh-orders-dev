import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
const ForgotPassword = (props) => {
  const [phone, setPhone] = useState('');
  const [passphrase, setPassphrase] = useState('');
  const [isPhoneValid, setPhoneValid] = useState(false);

  const handlePasswordCheck = () => {
    console.log('----', process.env.REACT_APP_PASSPHRASE);
    console.log('--phone--', phone);
    console.log('-passphrase---', passphrase === process.env.REACT_APP_PASSPHRASE);
    if (
      (phone === '8939394627' || phone === '9840226610') &&
      passphrase === process.env.REACT_APP_PASSPHRASE
    ) {
      setPhoneValid(true);
    }
  };

  const handleRemoveForgotPassword = () => {
    props.handleForgotPassword(false);
  };
  return (
    <div className={'loginBox'}>
      <div>WELCOME TO GH</div>
      <div className={'password-field'}>
        {!isPhoneValid ? (
          <>
            <TextField
              id="phone"
              required
              className={'password'}
              placeholder="Phone..."
              onChange={(event) => setPhone(event.target.value)}
              autoFocus
            />
            <TextField
              id="passphrase"
              required
              className={'password'}
              placeholder="Passphrase..."
              onChange={(event) => setPassphrase(event.target.value)}
              type="password"
              autoComplete="current-password"
            />
          </>
        ) : (
          <div className={'password-show'}>
            {process.env.REACT_APP_PASSWORD}
          </div>
        )}
      </div>
      <div className={'actions'}>
        {!isPhoneValid ? (
          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={handlePasswordCheck}
            >
              GET PASSWORD
            </Button>
          </div>
        ) : (
          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={handleRemoveForgotPassword}
            >
              LOGIN
            </Button>
          </div>
        )}
      </div>
    </div>
  );
};

export default ForgotPassword;
