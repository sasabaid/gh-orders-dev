import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import ItemsListing from './ItemsListing';
import TableCompanyInfo from './TableCompanyInfo';
import CompanyCreate from './CompanyCreate';
import { fuzzy_match } from './utils/GeneralUtils';
import { useDispatch, useSelector } from 'react-redux';
import actions from './actionTypes';

const COMPANY_META_DATA = {
  name: '',
  gst: '',
  address: '',
};

function CompanySelection({ setDataAfterSubmit, companyInfo }) {
  const [selectedItem, setSelectedItem] = useState({});
  const [filteredCompany, setFilteredCompany] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(1);
  const [company, setCompany] = useState([]);
  const [companyData, setCompanyData] = useState(null);
  const [openCreateCompany, setOpenCreateCompany] = useState(false);
  const [companyAction, setCompanyAction] = useState('');

  const dispatch = useDispatch();

  const { companies } = useSelector((state) => state.companyInfo);

  useEffect(() => {
    setCompany(companies);
    setFilteredCompany(companies);
  }, [companies]);

  const handleListItemClick = (event, item) => {
    setSelectedItem(item);
  };

  const handleRowSelection = ({ target }, rowId) => {
    setSelectedIndex(rowId);
    setSelectedItem({});
    if (target.value) {
      searchAndSetFilteredCompany(target.value);
    } else {
      setFilteredCompany(company);
    }
  };

  const searchAndSetFilteredCompany = (searchTerm) => {
    let filteredResult = [];
    company.forEach((company) => {
      const result = fuzzy_match(company.name, searchTerm);
      if (result !== '') {
        filteredResult.push(company);
      }
    });
    setFilteredCompany(filteredResult);
  };

  const handleCompanyNameChange = ({ target }) => {
    searchAndSetFilteredCompany(target.value);
  };

  const handleCompanyModify = (action, data) => {
    setOpenCreateCompany(true);
    setCompanyAction(action);
    setCompanyData(data);
  };

  const handleCloseOfCreateCompanys = () => {
    setOpenCreateCompany(false);
    setCompanyAction('');
    setCompanyData(null);
  };

  const handleCreateCompanySave = (data) => {
    if (companyAction === 'add') {
      dispatch({
        type: actions.SAVE_COMPANY,
        payload: data,
      });
    } else {
      dispatch({
        type: actions.UPDATE_COMPANY,
        payload: {
          id: companyData._id,
          modifiedInfo: data,
        },
      });
    }
    handleCloseOfCreateCompanys();
  };

  return (
    <>
      <div className={'bill-container'}>
        <div className={'item-listing-container'}>
          <ItemsListing
            title={'Company'}
            items={filteredCompany}
            selectedItem={selectedItem}
            handleListItemClick={handleListItemClick}
            handleEditItem={handleCompanyModify}
          />
          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={() => handleCompanyModify('add', COMPANY_META_DATA)}
            >
              Add Company
            </Button>
          </div>
        </div>

        <TableCompanyInfo
          selectedIndex={selectedIndex}
          selectedItem={selectedItem}
          handleCompanyNameChange={handleCompanyNameChange}
          handleRowSelection={handleRowSelection}
          setDataAfterSubmit={setDataAfterSubmit}
          billItems={companyInfo}
        />
      </div>
      {openCreateCompany && (
        <CompanyCreate
          handleCompanySave={handleCreateCompanySave}
          open={openCreateCompany}
          handleClose={handleCloseOfCreateCompanys}
          companyData={companyData}
        />
      )}
    </>
  );
}

export default CompanySelection;
