import React from 'react';
import { makeStyles } from '@mui/styles';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import EditRoundedIcon from '@mui/icons-material/EditRounded';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import IconButton from '@mui/material/IconButton';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: 300,
    overflow: 'scroll',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid',
  },
  itemTitle: {
    textAlign: 'center',
    fontSize: 24,
    backgroundColor: '#000',
    color: '#FFF',
  },
}));

function ItemsListing(props) {
  const { title, items, selectedItem, handleListItemClick, handleEditItem } =
    props;
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.itemTitle}>{title}</div>
      <div>
        <List component="nav">
          {items.map((item) => (
            <ListItem
              key={item._id}
              button
              selected={item._id === selectedItem?._id}
              onClick={(event) => handleListItemClick(event, item)}
            >
              <ListItemText primary={item.name} />
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  aria-label="edit"
                  onClick={() => handleEditItem('edit', item)}
                >
                  <EditRoundedIcon fontSize="large" />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </div>
    </div>
  );
}

export default ItemsListing;
