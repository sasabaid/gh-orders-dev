import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* updateProductSaga(action) {
  try {
    const api = `/products/${action.payload.id}`;
    const response = yield call(
      callFetchApi,
      api,
      {},
      'PUT',
      action.payload.modifiedInfo,
    );
    yield put({
      type: actions.UPDATE_PRODUCT_SUCCESS,
      response: response.data,
    });
    yield put({
      type: actions.ALL_PRODUCTS,
    });
  } catch (error) {
    yield put({
      type: actions.UPDATE_PRODUCT_FAILURE,
      error,
    });
  }
}
