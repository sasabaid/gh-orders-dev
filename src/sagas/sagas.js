import { takeLatest } from 'redux-saga/effects';
import actions from '../actionTypes';
import allOrdersSaga from './allOrdersSaga';
import allCompanysSaga from './allCompanysSaga';
import allProductsSaga from './allProductsSaga';
import saveOrderSaga from './saveOrderSaga';
import saveBillSaga from './saveBillSaga';
import saveCompanySaga from './saveCompanySaga';
import saveProductSaga from './saveProductSaga';
import searchOrderSaga from './searchOrderSaga';
import updateOrderSaga from './updateOrderSaga';
import updateProductSaga from './updateProductSaga';
import updateCompanySaga from './updateCompanySaga';
import deleteOrderSaga from './deleteOrderSaga';

export default function* saga() {
  yield takeLatest(actions.ALL_OPEN_ORDERS, allOrdersSaga);
  yield takeLatest(actions.ALL_COMPANYS, allCompanysSaga);
  yield takeLatest(actions.ALL_PRODUCTS, allProductsSaga);
  yield takeLatest(actions.SAVE_ORDER, saveOrderSaga);
  yield takeLatest(actions.SAVE_BILL, saveBillSaga);
  yield takeLatest(actions.SAVE_PRODUCT, saveProductSaga);
  yield takeLatest(actions.SAVE_COMPANY, saveCompanySaga);
  yield takeLatest(actions.SEARCH_ORDER, searchOrderSaga);
  yield takeLatest(actions.UPDATE_ORDER, updateOrderSaga);
  yield takeLatest(actions.UPDATE_PRODUCT, updateProductSaga);
  yield takeLatest(actions.UPDATE_COMPANY, updateCompanySaga);
  yield takeLatest(actions.DELETE_ORDER, deleteOrderSaga);
}
