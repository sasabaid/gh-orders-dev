import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* deleteOrderSaga(action) {
  try {
    const api = `/orders/${action.payload.id}`;
    const response = yield call(callFetchApi, api, {}, 'DELETE');
    yield put({
      type: actions.DELETE_ORDER_SUCCESS,
      response: response.data,
    });
  } catch (error) {
    yield put({
      type: actions.DELETE_ORDER_FAILURE,
      error,
    });
  }
}
