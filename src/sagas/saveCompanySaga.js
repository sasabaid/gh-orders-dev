import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* saveCompanySaga(action) {
  try {
    const api = '/companys';
    const response = yield call(callFetchApi, api, {}, 'POST', action.payload);
    yield put({
      type: actions.SAVE_COMPANY_SUCCESS,
      response: response.data,
    });
  } catch (error) {
    yield put({
      type: actions.SAVE_COMPANY_FAILURE,
      error,
    });
  }
}
