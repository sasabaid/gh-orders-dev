import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* allOrdersSaga(action) {
  try {
    const api = '/orders/open';
    const response = yield call(callFetchApi, api, action.payload, 'GET');
    yield put({
      type: actions.ALL_OPEN_ORDERS_SUCCESS,
      response: response.data,
    });
  } catch (error) {
    yield put({
      type: actions.ALL_OPEN_ORDERS_FAILURE,
      error,
    });
  }
}
