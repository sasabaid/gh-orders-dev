import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* allCompanysSaga(action) {
  try {
    const api = '/companys';
    const response = yield call(callFetchApi, api, action.payload, 'GET');
    yield put({
      type: actions.ALL_COMPANYS_SUCCESS,
      response: response.data,
    });
  } catch (error) {
    yield put({
      type: actions.ALL_COMPANYS_FAILURE,
      error,
    });
  }
}
