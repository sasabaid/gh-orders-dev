import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* allProductsSaga(action) {
  try {
    const api = '/products';
    const response = yield call(callFetchApi, api, action.payload, 'GET');
    yield put({
      type: actions.ALL_PRODUCTS_SUCCESS,
      response: response.data,
    });
  } catch (error) {
    yield put({
      type: actions.ALL_PRODUCTS_FAILURE,
      error,
    });
  }
}
