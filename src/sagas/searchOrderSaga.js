import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* searchOrderSaga(action) {
  try {
    const api = '/orders/search';
    const data = {
      searchData: action.payload,
    };
    const response = yield call(callFetchApi, api, data, 'GET');
    yield put({
      type: actions.SEARCH_ORDER_SUCCESS,
      response: response.data,
    });
  } catch (error) {
    yield put({
      type: actions.SEARCH_ORDER_FAILURE,
      error,
    });
  }
}
