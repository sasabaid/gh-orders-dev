import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* saveProductSaga(action) {
  try {
    const api = '/products';
    const response = yield call(callFetchApi, api, {}, 'POST', action.payload);
    yield put({
      type: actions.SAVE_PRODUCT_SUCCESS,
      response: response.data,
    });
  } catch (error) {
    yield put({
      type: actions.SAVE_PRODUCT_FAILURE,
      error,
    });
  }
}
