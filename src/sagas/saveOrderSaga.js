import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* saveOrderSaga(action) {
  try {
    const api = '/orders';
    const response = yield call(callFetchApi, api, {}, 'POST', action.payload);
    yield put({
      type: actions.SAVE_ORDER_SUCCESS,
      response: response.data,
    });
  } catch (error) {
    yield put({
      type: actions.SAVE_ORDER_FAILURE,
      error,
    });
  }
}
