import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* saveBillSaga(action) {
  try {
    const api = '/bills';
    const response = yield call(callFetchApi, api, {}, 'POST', action.payload);
    yield put({
      type: actions.SAVE_BILL_SUCCESS,
      response: response.data,
    });
  } catch (error) {
    yield put({
      type: actions.SAVE_BILL_FAILURE,
      error,
    });
  }
}
