import { put, call } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* updateOrderSaga(action) {
  try {
    const api = `/orders/${action.payload.id}`;
    const response = yield call(
      callFetchApi,
      api,
      {},
      'PUT',
      action.payload.modifiedInfo,
    );
    yield put({
      type: actions.UPDATE_ORDER_SUCCESS,
      response: response.data,
    });
    yield put({
      type: actions.ALL_OPEN_ORDERS,
    });
  } catch (error) {
    yield put({
      type: actions.UPDATE_ORDER_FAILURE,
      error,
    });
  }
}
