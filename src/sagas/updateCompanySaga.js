import { call, put } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* updateCompanySaga(action) {
  try {
    const api = `/companys/${action.payload.id}`;
    const response = yield call(
      callFetchApi,
      api,
      {},
      'PUT',
      action.payload.modifiedInfo,
    );
    yield put({
      type: actions.UPDATE_COMPANY_SUCCESS,
      response: response.data,
    });
    yield put({
      type: actions.ALL_COMPANYS,
    });
  } catch (error) {
    yield put({
      type: actions.UPDATE_COMPANY_FAILURE,
      error,
    });
  }
}
