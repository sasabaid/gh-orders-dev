import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import history from './history';
import MyLoadingComponent from './MyLoadingComponent';
import Loadable from 'react-loadable';

const AsyncHome = Loadable({
  loader: () => import(/* webpackChunkName: "App" */ './App'),
  loading: MyLoadingComponent,
});

const AsyncBill = Loadable({
  loader: () => import(/* webpackChunkName: "App" */ './CreateBill'),
  loading: MyLoadingComponent,
});
// eslint-disable-next-line
export default (props) => (
  <Router history={history}>
    <Switch>
      <Route exact path="/" component={AsyncHome} />
      <Route exact path="/bill" component={AsyncBill} />
    </Switch>
  </Router>
);
