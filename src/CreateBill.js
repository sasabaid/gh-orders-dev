import React, { useEffect, useState } from 'react';
import Bill from './Bill';
import CompanySelection from './CompanySelection';
import { useDispatch, useSelector } from 'react-redux';
import actions from './actionTypes';

function CreateBill() {
  const dispatch = useDispatch();
  const [showCompanyDetails, setShowCompanyDetails] = useState(false);

  const [data, setData] = useState({});
  const { bills } = useSelector((state) => state.billsInfo);
  const setDataAfterSubmit = (showCompanyPage, info) => {
    setShowCompanyDetails(showCompanyPage);

    if (info.companyInfo && !info.showBillPage) {
      dispatch({
        type: actions.SAVE_BILL,
        payload: { ...data, companyInfo: info.companyInfo },
      });
      setData({});
    } else {
      setData({ ...data, ...info });
    }
  };

  useEffect(() => {
    dispatch({
      type: actions.ALL_PRODUCTS,
    });
    dispatch({
      type: actions.ALL_COMPANYS,
    });
    dispatch({
      type: actions.ALL_BILLS,
    });
    return () => {
      setData({});
    };
  }, []);

  return (
    <>
      {!showCompanyDetails ? (
        <Bill
          setDataAfterSubmit={setDataAfterSubmit}
          billingItems={data.billingItems}
        />
      ) : (
        <CompanySelection
          setDataAfterSubmit={setDataAfterSubmit}
          companyInfo={data.companyInfo}
        />
      )}
    </>
  );
}

export default CreateBill;
