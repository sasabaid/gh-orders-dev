import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import actions from './actionTypes';
import Button from '@mui/material/Button';
import LinearProgress from '@mui/material/LinearProgress';
import OrderCreate from './OrderCreate';
import OrderTable from './OrderTable';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import * as R from 'ramda';

const columns = [
  { title: 'Name', field: 'name' },
  { title: 'Details', field: 'details' },
  { title: 'Order Date', field: 'updatedAt', type: 'date' },
];

const OrdersList = () => {
  const dispatch = useDispatch();

  const [orderType, setOrderType] = useState('');
  const [openCreateOrder, setOpenCreateOrder] = useState(false);
  const [orderFilter, setOrderFilter] = useState('from');
  const [orderAction, setOrderAction] = useState('');
  const [orderData, setOrderData] = useState(undefined);

  const { orders, allOdersInProgress } = useSelector(state => state.ordersReducer);

  useEffect(() => {
    dispatch({
      type: actions.ALL_OPEN_ORDERS,
    });
  }, []);

  const handleCreateOrders = (type, action) => {
    setOrderType(type);
    setOrderAction(action);
    setOpenCreateOrder(true);
    setOrderData(undefined);
  };

  const handleTableAction = (order) => {
    setOrderType(orderFilter);
    setOrderAction('edit');
    setOpenCreateOrder(true);
    setOrderData(order);
  };

  const handleCreateOrderSave = ({ name, details, status, closedDetails }) => {
    if (orderAction === 'create') {
      const payload = {
        type: orderType,
        name,
        details,
        status,
        closedDetails,
      };
      dispatch({
        type: actions.SAVE_ORDER,
        payload,
      });
    } else {
      const payload = {
        id: orderData._id,
        modifiedInfo: {
          type: orderData.type,
          name,
          details,
          status,
          closedDetails,
          updatedAt: new Date(),
        },
      };
      dispatch({
        type: actions.UPDATE_ORDER,
        payload,
      });
    }
    setOrderFilter(orderType);
    setOrderType('');
    setOpenCreateOrder(false);
  };

  const handleCloseOfCreateOrders = () => {
    setOpenCreateOrder(false);
  };

  return (
    <div className={'orderContainer'}>
      <div className="order-create">
        <div>
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleCreateOrders('from', 'create')}
          >
            FROM ORDERS
          </Button>
        </div>
        <div>
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleCreateOrders('to', 'create')}
          >
            TO ORDERS
          </Button>
        </div>
      </div>
      <div className="order-table">
        {allOdersInProgress && <LinearProgress />}
        <div className={'order-filter'}>
          <RadioGroup
            row
            aria-label="order-filter"
            name="orderFilter"
            value={orderFilter}
            onChange={(event) => setOrderFilter(event.target.value)}
          >
            <FormControlLabel
              value="from"
              control={<Radio />}
              label="From Orders"
            />
            <FormControlLabel
              value="to"
              control={<Radio />}
              label="To Orders"
            />
          </RadioGroup>
        </div>
        <div>
          <OrderTable
            handleAction={handleTableAction}
            orders={R.filter(R.propEq('type', orderFilter), orders)}
          />
        </div>
      </div>
      {openCreateOrder && (
        <OrderCreate
          handleOrderSave={handleCreateOrderSave}
          open={openCreateOrder}
          handleClose={handleCloseOfCreateOrders}
          orderType={orderType}
          orderAction={orderAction}
          orderData={orderData}
        />
      )}
    </div>
  );
};

export default OrdersList;
