import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Button from '@mui/material/Button';

const Login = (props) => {
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');

  const handlePasswordCheck = () => {
    if (password === process.env.REACT_APP_PASSWORD) {
      props.handlePasswordCheck(true);
      setPasswordError('');
    } else setPasswordError('Incorrect Password');
  };

  const handleEnterPress = (event) => {
    const code = event.keyCode ? event.keyCode : event.which;
    console.log(code);
    if (code === 13) {
      handlePasswordCheck();
    }
  };

  return (
    <div className={'loginBox'}>
      <div>WELCOME TO GH</div>
      <div className={'password-field'}>
        <TextField
          id="password"
          required
          className={'password'}
          placeholder="Password..."
          onChange={(event) => setPassword(event.target.value)}
          onKeyUp={handleEnterPress}
          type="password"
          autoComplete="current-password"
          autoFocus
        />
        <div className={'error'}>{passwordError}</div>
      </div>
      <div className={'actions'}>
        <div>
          <Link
            component="button"
            variant="body2"
            className={'forgot-password-link'}
            onClick={() => props.handleForgotPassword(true)}
          >
            Forgot Password
          </Link>
        </div>
        <div>
          <Button
            variant="contained"
            color="primary"
            onClick={handlePasswordCheck}
          >
            LOGIN
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Login;
