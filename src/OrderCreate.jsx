import React, { useEffect } from 'react';
import { makeStyles } from '@mui/styles';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    width: '100%',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '100%',
  },
  textArea: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '100%',
  },
  switch: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '100%',
  },
}));

const OrderCreate = (props) => {
  const [name, setName] = React.useState('');
  const [details, setDetails] = React.useState('');
  const [status, setStatus] = React.useState(0);
  const [closedDetails, setClosedDetails] = React.useState('');

  useEffect(() => {
    if (props.orderData) {
      setName(props.orderData.name);
      setDetails(props.orderData.details);
      setStatus(props.orderData.status);
      setClosedDetails(props.orderData.closedDetails);
    }
    // eslint-disable-next-line
  }, []);

  const classes = useStyles();

  const handleReset = () => {
    setName('');
    setDetails('');
    setClosedDetails('');
  };

  const handleOrderSave = () => {
    props.handleOrderSave({
      name,
      details,
      status,
      closedDetails,
    });
  };
  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      fullWidth={true}
      maxWidth={'sm'}
    >
      <DialogTitle id="alert-dialog-title">{`${props.orderType.toUpperCase()} ORDER DETAILS`}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          <span style={{ float: 'right' }}>
            Date::{' '}
            {props.orderData
              ? moment(new Date(props.orderData.updatedAt)).format('DD/MM/YYYY')
              : moment(new Date()).format('DD/MM/YYYY')}
          </span>
        </DialogContentText>
        <form className={classes.container} autoComplete="off">
          <div>
            <TextField
              id="name"
              className={classes.textField}
              label="Name"
              margin="normal"
              value={name}
              onChange={(event) => setName(event.target.value)}
              autoFocus
            />
          </div>
          <div>
            <TextField
              id="details"
              label="Details"
              multiline
              rows="4"
              value={details}
              className={classes.textArea}
              margin="normal"
              onChange={(event) => setDetails(event.target.value)}
              autoComplete="off"
            />
          </div>
          {props.orderAction === 'edit' && (
            <div>
              <FormControlLabel
                control={
                  <Switch
                    checked={status === 0}
                    onChange={(event) =>
                      setStatus(event.target.checked ? 0 : 1)
                    }
                    value={status}
                    color="primary"
                  />
                }
                labelPlacement="start"
                className={classes.switch}
                label={`Order Status (${status === 0 ? 'open' : 'close'})`}
              />
            </div>
          )}
          {props.orderAction === 'edit' && status === 1 && (
            <div>
              <TextField
                id="closedDetails"
                label="Closing Order Details"
                multiline
                rows="4"
                value={closedDetails}
                className={classes.textArea}
                margin="normal"
                onChange={(event) => setClosedDetails(event.target.value)}
                autoComplete="off"
              />
            </div>
          )}
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleReset} color="primary">
          RESET
        </Button>
        <Button
          onClick={handleOrderSave}
          color="primary"
          autoFocus
          disabled={name === '' || details === ''}
        >
          SAVE
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default OrderCreate;
