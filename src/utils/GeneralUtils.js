export function countOccurencesOfCharInString(string, char) {
  let re = new RegExp(char, 'g');
  return (string.match(re) || []).length;
}

export function doPatternTestForInputField(e, pattern) {
  if (pattern) {
    let str = e.target.value;
    pattern = new RegExp(pattern);
    let matched = pattern.test(str);
    return matched;
  }
  return true;
}

export function fuzzy_match(text, search) {
  search = search.replace(/ /g, '').toLowerCase();
  let tokens = [];
  let search_position = 0;

  // Go through each character in the text
  for (let n = 0; n < text.length; n++) {
    let text_char = text[n];
    // if we match a character in the search, highlight it
    if (
      search_position < search.length &&
      text_char.toLowerCase() === search[search_position]
    ) {
      search_position += 1;
    }
    tokens.push(text_char);
  }

  if (search_position !== search.length) {
    return '';
  }
  return tokens.join('');
}
