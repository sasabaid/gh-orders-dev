import React, { useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
import { makeStyles } from '@mui/styles';
import AddCircleOutlinedIcon from '@mui/icons-material/AddCircleOutlined';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import IconButton from '@mui/material/IconButton';

const useStyles = makeStyles({
  underline: {
    '&&&:before': {
      borderBottom: 'none',
    },
    '&&:after': {
      borderBottom: 'none',
    },
  },
});

function TableCart({
  selectedItem,
  selectedIndex,
  handleRowSelection,
  handleProductNameChange,
  setDataAfterSubmit,
  billItems,
}) {
  const classes = useStyles();
  const [maxItems, setMaxItems] = useState(5);
  const [billingItems, setBillingItems] = useState([]);

  useEffect(() => {
    if (billItems) setBillingItems(billItems);
    else {
      let items = [];
      for (let i = 0; i < 5; i++) {
        items.push({
          id: i + 1,
          name: '',
          hsn: '',
          qty: '',
          rate: '',
        });
      }
      setBillingItems(items);
    }
    return () => {
      setBillingItems([]);
    };
  }, [billItems]);

  useEffect(() => {
    if (billingItems.length > 0 && selectedItem.name) {
      let items = [...billingItems];
      items[selectedIndex - 1].name = selectedItem.name;
      items[selectedIndex - 1].hsn = selectedItem.hsn;
      setBillingItems(items);
    }
  }, [selectedItem, selectedIndex]);

  const addRow = () => {
    let items = [...billingItems];
    items.push({
      id: maxItems + 1,
      name: '',
      hsn: '',
      qty: '',
      rate: '',
    });
    setMaxItems(maxItems + 1);
    setBillingItems(items);
  };

  const handleSubmit = () => {
    setDataAfterSubmit(true, { billingItems });
  };

  const handleInputChange = (event, id) => {
    const { target } = event;
    const field = target.name;
    const value = target.value;
    let items = [...billingItems];
    items[id - 1][field] = value;
    setBillingItems(items);
    if (field === 'name') handleProductNameChange(event);
  };

  const headers = ['S.No.', 'Product Name', 'HSN', 'Quantity', 'Rate'];

  return (
    <div className={'table-cart'}>
      <div className={'table-cart-header'}>
        {headers.map((header) => (
          <div key={header} className={'table-cart-header-column'}>
            {header}
          </div>
        ))}
      </div>
      <div className={'table-cart-body'}>
        {billingItems.map((billingItem) => (
          <div key={billingItem.id} className={'table-cart-body-row'}>
            <div className={'table-cart-body-column'}>{billingItem.id}</div>
            <div className={'table-cart-body-column'}>
              <TextField
                name={'name'}
                fullWidth
                onFocus={(event) => handleRowSelection(event, billingItem.id)}
                onChange={(event) => handleInputChange(event, billingItem.id)}
                value={billingItem.name}
                InputProps={{ classes }}
              />
            </div>
            <div className={'table-cart-body-column'}>
              <TextField
                name={'hsn'}
                fullWidth
                value={billingItem.hsn}
                InputProps={{ classes }}
                onChange={(event) => handleInputChange(event, billingItem.id)}
              />
            </div>
            <div className={'table-cart-body-column'}>
              <TextField
                name={'qty'}
                fullWidth
                value={billingItem.qty}
                InputProps={{ classes }}
                onChange={(event) => handleInputChange(event, billingItem.id)}
              />
            </div>
            <div className={'table-cart-body-column'}>
              <TextField
                name={'rate'}
                fullWidth
                value={billingItem.rate}
                InputProps={{ classes }}
                onChange={(event) => handleInputChange(event, billingItem.id)}
              />
            </div>
          </div>
        ))}
      </div>
      <div className={'table-cart-actions'}>
        <IconButton aria-label="addrow" onClick={addRow}>
          <AddCircleOutlinedIcon fontSize="large" />
        </IconButton>
        <IconButton aria-label="addrow" onClick={handleSubmit}>
          <CheckCircleIcon fontSize="large" />
        </IconButton>
      </div>
    </div>
  );
}

export default TableCart;
