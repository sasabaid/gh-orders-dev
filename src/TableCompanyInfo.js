import React, { useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
import { makeStyles } from '@mui/styles';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import IconButton from '@mui/material/IconButton';

const useStyles = makeStyles({
  underline: {
    '&&&:before': {
      borderBottom: 'none',
    },
    '&&:after': {
      borderBottom: 'none',
    },
  },
});

function TableCompanyInfo({
  selectedItem,
  selectedIndex,
  handleRowSelection,
  handleCompanyNameChange,
  setDataAfterSubmit,
  billItems,
}) {
  const classes = useStyles();
  const [billingItems, setBillingItems] = useState([]);

  useEffect(() => {
    if (billItems) setBillingItems(billItems);
    else {
      let items = [];
      for (let i = 0; i < 1; i++) {
        items.push({
          id: i + 1,
          name: '',
          address: '',
          gst: '',
        });
      }
      setBillingItems(items);
    }
    return () => {
      setBillingItems([]);
    };
  }, [billItems]);

  useEffect(() => {
    if (billingItems.length > 0 && selectedItem.name) {
      let items = [...billingItems];
      items[selectedIndex - 1].name = selectedItem.name;
      items[selectedIndex - 1].address = selectedItem.address;
      items[selectedIndex - 1].gst = selectedItem.gst;
      setBillingItems(items);
    }
  }, [selectedItem, selectedIndex]);

  const handleSubmit = (showBillPage) => {
    setDataAfterSubmit(false, { companyInfo: billingItems, showBillPage });
  };

  const handleInputChange = (event, id) => {
    const { target } = event;
    const field = target.name;
    const value = target.value;
    let items = [...billingItems];
    items[id - 1][field] = value;
    setBillingItems(items);
    if (field === 'name') handleCompanyNameChange(event);
  };

  const headers = ['S.No.', 'Company Name', 'Address', 'GST'];

  return (
    <div className={'table-cart'}>
      <div className={'table-cart-header'}>
        {headers.map((header) => (
          <div key={header} className={'table-cart-header-column'}>
            {header}
          </div>
        ))}
      </div>
      <div className={'table-cart-body'}>
        {billingItems.map((billingItem) => (
          <div key={billingItem.id} className={'table-cart-body-row'}>
            <div className={'table-cart-body-column'}>{billingItem.id}</div>
            <div className={'table-cart-body-column'}>
              <TextField
                name={'name'}
                fullWidth
                onFocus={(event) => handleRowSelection(event, billingItem.id)}
                onChange={(event) => handleInputChange(event, billingItem.id)}
                value={billingItem.name}
                InputProps={{ classes }}
              />
            </div>
            <div className={'table-cart-body-column'}>
              <TextField
                name={'address'}
                fullWidth
                value={billingItem.address}
                InputProps={{ classes }}
                onChange={(event) => handleInputChange(event, billingItem.id)}
              />
            </div>
            <div className={'table-cart-body-column'}>
              <TextField
                name={'gst'}
                fullWidth
                value={billingItem.gst}
                InputProps={{ classes }}
                onChange={(event) => handleInputChange(event, billingItem.id)}
              />
            </div>
          </div>
        ))}
      </div>
      <div className={'table-cart-actions'}>
        <IconButton aria-label="addrow" onClick={() => handleSubmit(true)}>
          <ArrowBackOutlinedIcon fontSize="large" />
        </IconButton>
        <IconButton
          aria-label="submit-data"
          onClick={() => handleSubmit(false)}
        >
          <CheckCircleIcon fontSize="large" />
        </IconButton>
      </div>
    </div>
  );
}

export default TableCompanyInfo;
