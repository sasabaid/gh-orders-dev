import React, { useEffect, useState } from 'react';
import { makeStyles } from '@mui/styles';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    width: '100%',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '100%',
  },
}));

const ProductCreate = ({
  open,
  productData,
  handleClose,
  handleProductSave,
}) => {
  const [name, setName] = useState('');
  const [hsn, setHsn] = useState('');

  useEffect(() => {
    if (productData) {
      setName(productData.name);
      setHsn(productData.hsn);
    }
  }, [productData]);

  const classes = useStyles();

  const handleReset = () => {
    setName('');
    setHsn('');
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      fullWidth={true}
      maxWidth={'sm'}
    >
      <DialogTitle id="alert-dialog-title">{'PRODUCT DETAILS'}</DialogTitle>
      <DialogContent>
        <form className={classes.container} autoComplete="off">
          <div>
            <TextField
              id="name"
              className={classes.textField}
              label="Name"
              margin="normal"
              value={name}
              onChange={(event) => setName(event.target.value)}
              autoFocus
            />
          </div>
          <div>
            <TextField
              id="hsn"
              label="HSN"
              value={hsn}
              className={classes.textField}
              margin="normal"
              onChange={(event) => setHsn(event.target.value)}
              autoComplete="off"
            />
          </div>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleReset} color="primary">
          RESET
        </Button>
        <Button
          onClick={() =>
            handleProductSave({
              name,
              hsn,
            })
          }
          color="primary"
          autoFocus
          disabled={name === '' || hsn === ''}
        >
          SAVE
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ProductCreate;
